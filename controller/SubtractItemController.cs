﻿using System;
using ProjectARES.dao;
using ProjectARES.model;

namespace ProjectARES.controller
{
    public class SubtractItemController
    {

        /// <summary>
        /// Buy an Item
        /// </summary>
        /// <param name="itemid">the item to buy</param>
        /// <param name="quantity">the quantity to buy</param>
        /// <param name="storeId">the store from which to buy</param>
        /// <returns>A status message of the purchase</returns>
        public void InventoryDaoSub(int ItemID)
        {
            var inventoryDao = new InventoryDao();

            var inventory = inventoryDao.GetInventoryByStore(1);

            InventoryController inventoryController = new InventoryController();

            if(ItemID <= inventory.Count && ItemID != 0)
            {
                //delete inventory item

                var inventoryItem = GetInventoryById(ItemID, inventoryDao);

                //delete inventory
                inventoryDao.RemoveInventory(1, ItemID);

                //reread record from db
                var updatedInventoryItem = GetInventoryById(ItemID, inventoryDao);

                Console.WriteLine("\n");

                if (updatedInventoryItem != inventoryItem)
                {
                    Console.WriteLine("ItemID Deleted: {0}", ItemID);
                }
                else
                {
                    Console.WriteLine("ItemID Not Deleted: {0}", ItemID);
                }

            }
            else
                Console.WriteLine("Error ItemID {0} does not exist", ItemID);

        }


        /// <summary>
        /// get the inventory for the specified item id
        /// </summary>
        /// <param name="itemId">the item id</param>
        /// <param name="dao">the inventory dao</param>
        /// <returns>the inventory for that item id</returns>
        static Inventory GetInventoryById(int inventoryId, InventoryDao dao)
        {
            var results = dao.GetInventoryByStore(1);
            return results[inventoryId];
        }




    }
}