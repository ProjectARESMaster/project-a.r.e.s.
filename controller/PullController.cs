﻿using System.Collections.Generic;
using ProjectARES.dao;
using ProjectARES.model;
using ProjectARES.utils;

namespace ProjectARES.controller
{
    public class PullController
    {
        private readonly InventoryDao inventoryDao = new InventoryDao();

        /// <summary>
        /// Run the pulls for a store. Restock items from the back to the floor. 
        /// </summary>
        /// <param name="storeId">the store</param>
        /// <returns>list of pulled inventories</returns>
        public IList<Inventory> RunPulls(int storeId)
        {
            IList<Inventory> inventoryList = inventoryDao.GetInventoryByStore(storeId);

            IList<Inventory> pulledInventories = InventoryUtils.PullInventory(inventoryList);

            inventoryDao.UpdateInventory(pulledInventories);

            return pulledInventories;
        }
    }
}