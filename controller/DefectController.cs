﻿using System.Text;
using ProjectARES.dao;
using ProjectARES.utils;

namespace ProjectARES.controller
{
    public class DefectController
    {
        InventoryDao inventoryDao = new InventoryDao();

        
        /// <summary>
        /// Defect an item
        /// </summary>
        /// <param name="itemid">the item to defect</param>
        /// <param name="quantity">the quantity to defect</param>
        /// <param name="storeId">the store from which to defect</param>
        /// <returns>A status message of the defection</returns>
        public string DefectItem(int itemid, int quantity, int storeId)
        {
            StringBuilder messageBuilder = new StringBuilder();
            var inventory = inventoryDao.GetInventoryByItemId(itemid, storeId);

            //item isn't available 
            if (!InventoryUtils.IsItemAvailable(inventory))
            {
                messageBuilder.AppendFormat("{0} is out of stock already.", inventory.Item.Name);
                return messageBuilder.ToString();
            }

            //item isn't available at quantity 
            if (!InventoryUtils.IsItemAvailable(inventory, quantity))
            {
                messageBuilder.AppendFormat("{0} is available. but not in that quantity.\n", inventory.Item.Name);
            }

            //Prevents inventory from going negative
            if (inventory.InBackCount >= quantity)
            {
                inventory.InBackCount -= quantity;
            }
            else
            {
                messageBuilder.AppendFormat("Rejected request to salvage {0}, back stock inventory {1} \n", inventory.Item.Name, inventory.InBackCount);
            }

            //Mutates inventory
            inventoryDao.UpdateInventory(inventory);

            messageBuilder.AppendFormat("\nItem Defected: {0}\n Quantity: {1}", inventory.Item.Name, quantity);

            return messageBuilder.ToString();
        }

    }
}