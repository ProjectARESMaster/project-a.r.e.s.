﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectARES.dao;
using ProjectARES.model;
using ProjectARES.utils;

namespace ProjectARES.controller
{
    public class SalvageController
    {
        InventoryDao inventoryDao = new InventoryDao();

        
        /// <summary>
        /// Buy an Item
        /// </summary>
        /// <param name="itemid">the item to buy</param>
        /// <param name="quantity">the quantity to buy</param>
        /// <param name="storeId">the store from which to buy</param>
        /// <returns>A status message of the purchase</returns>
        public string SalvageItem(int itemid, int quantity, int storeId)
        {
            StringBuilder messageBuilder = new StringBuilder();
            var inventory = inventoryDao.GetInventoryByItemId(itemid, storeId);

            //item isn''t available 
            if (!InventoryUtils.IsItemAvailable(inventory))
            {
                messageBuilder.AppendFormat("{0} is out of stock.", inventory.Item.Name);
                return messageBuilder.ToString();
            }

            //item isn''t available at quantity 
            if (!InventoryUtils.IsItemAvailable(inventory, quantity))
            {
                messageBuilder.AppendFormat("{0} is available. but not in that quantity.\n", inventory.Item.Name);
            }

            //Prevents inventory from going negative
            if (inventory.InBackCount  >= quantity)
            {
                inventory.InBackCount -= quantity;
            }
            else
            {
                messageBuilder.AppendFormat("Rejected request to salvage {0}, back stock inventory {1} \n", inventory.Item.Name,inventory.InBackCount);
            }
            

            //mutates inventory
            inventoryDao.UpdateInventory(inventory);

            messageBuilder.AppendFormat("\nItem Salvage: {0}\n Quantity: {1}", inventory.Item.Name, quantity);

            return messageBuilder.ToString();
        }

    }
}