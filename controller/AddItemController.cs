﻿using System;
using System.Text;
using ProjectARES.dao;
using ProjectARES.model;

using static ProjectARES.utils.MenuUtils;

namespace ProjectARES.controller
{
    public class AddItemController
    {

        /// <summary>
        /// Buy an Item
        /// </summary>
        /// <param name="itemid">the item to buy</param>
        /// <param name="quantity">the quantity to buy</param>
        /// <param name="storeId">the store from which to buy</param>
        /// <returns>A status message of the purchase</returns>
        public void InventoryDaoAdd(string ItemName, string FillGroup, int SalesFloor, int SalesFloorCount, int InBackCount)
        {
            var inventoryDao = new InventoryDao();


            //tried to put too many items on the sales floor
            while (SalesFloorCount > SalesFloor)
            {
                WriteSalesFloorError();

                SalesFloor = ErrorCatchSalesFloor();
                SalesFloorCount = ErrorCatchSalesFloorCount();
            }

            Console.WriteLine("Current last two Inventory ID's");

            printLastTwoInventory(inventoryDao);


            Console.WriteLine("Adding Inventory\n");

            Item newItem = new Item(ItemName, FillGroup);
            Inventory newInventory = new Inventory(1, newItem, SalesFloor, SalesFloorCount, InBackCount);


            inventoryDao.AddInventory(newInventory);

            Console.WriteLine("Updated last two Inventory ID's");

            printLastTwoInventory(inventoryDao);


        }

        /// <summary>
        /// write the error message for invalid sales floor amounts
        /// </summary>
        private static void WriteSalesFloorError()
        {
            Console.WriteLine("Error: Sales Floor Value is greater than what can fit on the shelf.");
            Console.WriteLine("Please Re-Enter the values!");
            PressAnyKey();
            Console.Clear();
        }

        /// <summary>
        /// print the last 2 inventory items to the console
        /// </summary>
        /// <param name="inventoryDao">the InventoryDao</param>
        /// <returns></returns>
        public static string printLastTwoInventory(InventoryDao inventoryDao)
        {
            var inventory = inventoryDao.GetInventoryByStore(1);
            StringBuilder messageBuilder = new StringBuilder();

            Console.WriteLine(inventory[inventory.Count - 2]);
            Console.WriteLine(inventory[inventory.Count - 1]);
            Console.WriteLine("\n\n");


            return messageBuilder.ToString();
        }

        /// <summary>
        /// reprompt for the sales floor size
        /// </summary>
        /// <returns></returns>
        public static int ErrorCatchSalesFloor()
        {
            Console.WriteLine("How many fit on the Sales Floor?");
            var SalesFloor = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            return SalesFloor;
        }

        /// <summary>
        /// reprompt for the number of items on the sales floor
        /// </summary>
        /// <returns></returns>
        public static int ErrorCatchSalesFloorCount()
        {
            Console.WriteLine("How many are on the Sales Floor?");
            var SalesFloorCount = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            return SalesFloorCount;
        }

    }
}