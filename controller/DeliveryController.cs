﻿using System.Collections.Generic;
using System.Linq;
using ProjectARES.dao;
using ProjectARES.model;

namespace ProjectARES.controller
{
    public class DeliveryController
    {
        private readonly DeliveryDao deliveryDao = new DeliveryDao();
        private readonly SalesDao salesDao = new SalesDao();
        private readonly InventoryDao inventoryDao = new InventoryDao();

        /// <summary>
        /// Generate a restock order and accept delivery
        /// </summary>
        /// <param name="storeId">the store</param>
        /// <returns>The new delivery</returns>
        public Delivery DeliveryTruck(int storeId)
        {
            //get most recent delivery
            var deliveries = deliveryDao.GetDeliveriesByStore(storeId);
            Delivery mostRecentDelivery = deliveries.OrderByDescending(x => x.DeliveryTime)
                                                         .First();

            //get items sold after most recent delivery
            var sales = salesDao.GetSalesByStore(1);
            var SoldItems = sales.Where(sale => sale.SaleTime >= mostRecentDelivery.DeliveryTime)
                                        .ToList();

            //create delivery
            IList<DeliveredItem> DeliveredItems = SoldItems.Select(sale => new DeliveredItem(sale.Item))
                                                           .ToList();

            var DeliveryTruck = new Delivery(storeId, System.DateTime.Now, DeliveredItems);

            deliveryDao.AddDeliveries(DeliveryTruck);
            updateInventory(DeliveryTruck, storeId);

            return DeliveryTruck;
        }

        /// <summary>
        /// update the inventory from the new delivery
        /// </summary>
        /// <param name="delivery">the delivery</param>
        /// <param name="storeId">the store</param>
        private void updateInventory(Delivery delivery, int storeId)
        {
            foreach (var deliveredItem in delivery.DeliveredItems)
            {
                var inventory = inventoryDao.GetInventoryByItemId(deliveredItem.Item.Id, storeId);
                inventory.InBackCount += 1;

                inventoryDao.UpdateInventory(inventory);
            }
        }
    }
}