﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectARES.dao;
using ProjectARES.model;
using ProjectARES.utils;

namespace ProjectARES.controller
{
    public class RegisterController
    {
        InventoryDao inventoryDao = new InventoryDao();
        SalesDao salesDao = new SalesDao();

        /// <summary>
        /// Buy an Item
        /// </summary>
        /// <param name="itemid">the item to buy</param>
        /// <param name="quantity">the quantity to buy</param>
        /// <param name="storeId">the store from which to buy</param>
        /// <returns>A status message of the purchase</returns>
        public string BuyItem(int itemid, int quantity, int storeId)
        {
            StringBuilder messageBuilder = new StringBuilder();
            var inventory = inventoryDao.GetInventoryByItemId(itemid, storeId);
            
            //item isn''t available 
            if (!InventoryUtils.IsItemAvailable(inventory))
            {
                messageBuilder.AppendFormat("{0} is out of stock.", inventory.Item.Name);
                return messageBuilder.ToString();
            }

            //item isn''t available at quantity 
            if (!InventoryUtils.IsItemAvailable(inventory, quantity))
            {
                messageBuilder.AppendFormat("{0} is available. but not in that quantity.\n", inventory.Item.Name);
            }

            //mutates inventory
            int numberOfItemsSold = InventoryUtils.RemoveItemFromInventory(inventory, quantity);

            inventoryDao.UpdateInventory(inventory);

            SellItem(inventory.Item, storeId, numberOfItemsSold);

            messageBuilder.AppendFormat("\nItem sold: {0}\n Quantity: {1}", inventory.Item.Name, numberOfItemsSold);

            return messageBuilder.ToString();
        }

        /// <summary>
        /// send the sales to the database
        /// </summary>
        /// <param name="item">item sold</param>
        /// <param name="storeId">the store</param>
        /// <param name="soldQuantity">number of items sold</param>
        private void SellItem(Item item, int storeId, int soldQuantity)
        {
            List<Sales> sales = new List<Sales>(soldQuantity);

            for (int i = 0; i < soldQuantity; i++)
            {
                sales.Add(new Sales(item, storeId, DateTime.Now));
            }

            salesDao.InsertSales(sales);
        }
    }
}