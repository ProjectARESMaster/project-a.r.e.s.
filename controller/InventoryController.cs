﻿using System.Collections.Generic;
using ProjectARES.dao;
using ProjectARES.model;


namespace ProjectARES.controller
{
    public class InventoryController
    {
        private readonly InventoryDao inventoryDao = new InventoryDao();

        /// <summary>
        /// Get the inventory for a Store
        /// </summary>
        /// <param name="storeId">the store</param>
        /// <returns>the stores inventory</returns>
        public IList<Inventory> Inventory(int storeId)
        {
            var inventoryDao = new InventoryDao();
            return inventoryDao.GetInventoryByStore(storeId);
        }
    }
}
