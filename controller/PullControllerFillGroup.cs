﻿using System.Collections.Generic;
using ProjectARES.dao;
using ProjectARES.model;
using ProjectARES.utils;
using System.Linq;

namespace ProjectARES.controller
{
    public class PullControllerFillGroup
    {
        private readonly InventoryDao inventoryDao = new InventoryDao();

        /// <summary>
        /// Run the pulls for a store. Restock items from the back to the floor. 
        /// </summary>
        /// <param name="storeId">the store</param>
        /// <returns>list of pulled inventories</returns>
        public IList<Inventory> RunSortPulls(int storeId, string userInput)
        {
            IList<Inventory> inventoryList = inventoryDao.GetInventoryByStore(storeId);


            IList<Inventory> sortedItems = inventoryList.Where(s => s.Item.FillGroup == userInput).ToList();


            IList<Inventory> pulledInventories = InventoryUtils.PullInventory(sortedItems);
            inventoryDao.UpdateInventory(pulledInventories);
            return pulledInventories;

        }
    }
}
