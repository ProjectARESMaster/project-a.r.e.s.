﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectARES.dao;
using ProjectARES.model;
using ProjectARES.utils;

namespace ProjectARES.controller
{
    public class ReturnController
    {
        InventoryDao inventoryDao = new InventoryDao();

        
        /// <summary>
        /// Buy an Item
        /// </summary>
        /// <param name="itemid">the item to buy</param>
        /// <param name="quantity">the quantity to buy</param>
        /// <param name="storeId">the store from which to buy</param>
        /// <returns>A status message of the purchase</returns>
        public string ReturnItem(int itemid, int quantity, int storeId)
        {
            StringBuilder messageBuilder = new StringBuilder();
            var inventory = inventoryDao.GetInventoryByItemId(itemid, storeId);
            inventory.InBackCount += quantity;

            //mutates inventory
            inventoryDao.UpdateInventory(inventory);

            messageBuilder.AppendFormat("\nItem Returned: {0}\n Quantity: {1}", inventory.Item.Name, quantity);

            return messageBuilder.ToString();
        }

    }
}