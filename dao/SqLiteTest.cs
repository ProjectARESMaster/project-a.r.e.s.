﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectARES.model;


namespace ProjectARES.dao
{
    /// <summary>
    /// Integration tests for the database
    /// </summary>
    class SqLiteTest
    {


        static void Maine(string[] args)
        {

            new DatabaseBuilder().BuildDatabase();

            //            SalesDaoReadTest();
            //            SalesDaoUpdateTest();
            //            InventoryDaoReadTest();
            //            InventoryDaoByItemIdReadTest();
            //            InventoryDaoUpdateTest();
            //            InventoryDaoRemoveTest();
            InventoryDaoAddTest();
            //            DeliveryDaoReadTest();
            //            DeliveryDaoInsertTest();
            //            SalvageDaoReadTest();
            //            SalvageDaoUpdateTest();


        }

        static void SalesDaoReadTest()
        {
            var salesDao = new SalesDao();

            var results = salesDao.GetSalesByStore(1);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        static void SalesDaoUpdateTest()
        {
            Console.WriteLine("before sales");
            SalesDaoReadTest();
            SalesDao salesDao = new SalesDao();


            var newSales = new List<Sales>{new Sales(getRandomItem(), 1, DateTime.Now),
                                           new Sales(getRandomItem(), 1, DateTime.Now)
            };

            salesDao.InsertSales(newSales);

            Console.WriteLine("\nafter sales");
            SalesDaoReadTest();
        }   
        static void SalvageDaoReadTest()
        {
            var salvageDao = new SalvageDao();

            var results = salvageDao.GetSalvagesByStore(1);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        static void SalvageDaoUpdateTest()
        {
            Console.WriteLine("before salvage");
            SalvageDaoReadTest();
            SalvageDao salvageDao = new SalvageDao();


            var newSales = new List<Salvage>{new Salvage(getRandomItem(), 1, DateTime.Now),
                                           new Salvage(getRandomItem(), 1, DateTime.Now)
            };

            salvageDao.InsertSalvages(newSales);

            Console.WriteLine("\nafter salvage");
            SalvageDaoReadTest();
        }

        static Item getRandomItem()
        {
            InventoryDao inventoryDao = new InventoryDao();
            Random random = new Random();
            return GetInventoryById(random.Next(1, 50), inventoryDao).Item;
        }

        static void InventoryDaoReadTest()
        {
            var inventoryDao = new InventoryDao();

            var results = inventoryDao.GetInventoryByStore(1);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }
        static void InventoryDaoByItemIdReadTest()
        {
            var inventoryDao = new InventoryDao();

            var result = inventoryDao.GetInventoryByItemId(4, 1);

            Console.WriteLine(result);
        }

        static void InventoryDaoUpdateTest()
        {
            var inventoryDao = new InventoryDao();
            Random random = new Random();

            //pick random inventory
            var inventoryId = random.Next(50);

            var inventoryItem = GetInventoryById(inventoryId, inventoryDao);
            Console.WriteLine("inventory before update: " + inventoryItem);

            //update in back count
            inventoryItem.InBackCount = random.Next(1, 10);
            inventoryDao.UpdateInventory(new List<Inventory>{inventoryItem});

            //reread record from db
            var updatedInventoryItem = GetInventoryById(inventoryId, inventoryDao);
            Console.WriteLine("inventory after update:  " + updatedInventoryItem);
        }

        static void InventoryDaoRemoveTest()
        {
            var inventoryDao = new InventoryDao();
            Random random = new Random();

            //pick random inventory
            var inventoryId = random.Next(50);

            var inventoryItem = GetInventoryById(inventoryId, inventoryDao);
            Console.WriteLine("inventory to delete:  " + inventoryItem);

            //delete inventory
            inventoryDao.RemoveInventory(inventoryItem);

            //reread record from db
            var updatedInventoryItem = GetInventoryById(inventoryId, inventoryDao);

            if (updatedInventoryItem != inventoryItem)
            {
                Console.WriteLine("item deleted");
            }
            else
            {
                Console.WriteLine("item Not deleted");
            }
        }

        static void InventoryDaoAddTest()
        {
            var inventoryDao = new InventoryDao();

            printLastTwoInventory(inventoryDao);
            Console.WriteLine("Adding Inventory\n");

            Item newItem = new Item("new item", "fill group b");
            Inventory newInventory = new Inventory(1, newItem, 5, 2, 6);
            

            inventoryDao.AddInventory(newInventory);

            printLastTwoInventory(inventoryDao);
        }

        static void printLastTwoInventory(InventoryDao inventoryDao)
        {
            var inventory = inventoryDao.GetInventoryByStore(1);

            Console.WriteLine(inventory[inventory.Count - 2]);
            Console.WriteLine(inventory[inventory.Count - 1]);
            Console.WriteLine("\n\n");
        }

        static Inventory GetInventoryById(int inventoryId, InventoryDao dao)
        {
            var results = dao.GetInventoryByStore(1);
            return results[inventoryId];
        }

        static void DeliveryDaoReadTest()
        {
            DeliveryDao deliveryDao = new DeliveryDao();

            deliveryDao.GetDeliveriesByStore(1).ToList().ForEach(x => Console.WriteLine(x));
        }

        static void DeliveryDaoInsertTest()
        {
            //print current deliveries
            DeliveryDaoReadTest();
            Console.WriteLine("----------------------------------------------------------------------");

            //update deliveries 
            DeliveryDao deliveryDao = new DeliveryDao();


            Delivery newDelivery = new Delivery(1,
                                                DateTime.Now,
                                                new List<DeliveredItem>
                                                {
                                                    new DeliveredItem(getRandomItem()), 
                                                    new DeliveredItem(getRandomItem()),
                                                    new DeliveredItem(getRandomItem())
                                                });

            deliveryDao.AddDeliveries(newDelivery);


            //print updated deliveries
            Console.WriteLine("----------------------------------------------------------------------");
            DeliveryDaoReadTest();
        }

    }

}
