﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using ProjectARES.model;

namespace ProjectARES.dao
{
    public class SalesDao : SQLiteDao
    {
        /// <summary>
        /// Get all the sales for a store
        /// </summary>
        /// <param name="storeId">the store</param>
        /// <returns></returns>
        public IList<Sales> GetSalesByStore(int storeId)
        {
                string query = " select i.id,               " + //0
                               "        i.name,             " + //1 
                               "        i.fill_group,       " + //2
                               "        s.id,               " + //3
                               "        s.store_id,         " + //4
                               "        s.sale_timestamp    " + //5
                               "   from Item i, Sales s     " +
                               "  where i.id = s.item_id    " +
                               "    and s.store_id = " + storeId + ";";


                return RunQuery(query, GetSalesByStoreMapper);
        }

        /// <summary>
        /// A function to map the query results to a sale
        /// </summary>
        /// <param name="dataReader">the data reader from the query</param>
        /// <returns></returns>
        private Sales GetSalesByStoreMapper(SQLiteDataReader dataReader)
        {
            Item item = new Item(dataReader.GetInt32(0),
                                 dataReader.GetString(1),
                                 dataReader.GetString(2));

            return new Sales(dataReader.GetInt32(3),
                             item,
                             dataReader.GetInt32(4),
                             ToDateTime(dataReader.GetString(5)));
        }

        /// <summary>
        /// Insert new sales into the database
        /// </summary>
        /// <param name="sales">the sales to insert</param>
        public void InsertSales(IList<Sales> sales)
        {
            var query = " INSERT into Sales(item_id, store_id, sale_timestamp) " +
                        " VALUES ";

            var saleTime = DateTime.Now;

            var queryValues = string.Join(",",  sales.Select( x => BuildInsert(x, saleTime)).ToList());


            RunCommand(query + queryValues);
        }

        /// <summary>
        /// build the values clause for a Sale
        /// </summary>
        /// <param name="sale"></param>
        /// <param name="saleTime"></param>
        /// <returns></returns>
        private string BuildInsert(Sales sale, DateTime saleTime)
        {
            return " ( " + sale.Item.Id + ", " + sale.StoreId + ", \"" + ToIsoDateString(saleTime) + "\" ) ";
        }
    }
}