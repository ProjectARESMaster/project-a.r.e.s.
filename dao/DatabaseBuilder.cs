﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace ProjectARES.dao
{
    /// <summary>
    /// Build the database for the project
    /// </summary>
    public class DatabaseBuilder : SQLiteDao
    {
        /// <summary>
        /// build the database
        /// </summary>
        public void BuildDatabase()
        {
            DropTables();
            CreateTables();
            InsertData();
            InsertTestData();
            ReadData();
        }

        /// <summary>
        /// Create the tables
        /// </summary>
        private void CreateTables()
        {
            Console.WriteLine("creating tables");
            string store = "CREATE TABLE Store (id Integer PRIMARY KEY, number Integer);";
            string item = "CREATE TABLE Item ( id Integer PRIMARY KEY, name Text NOT NULL, fill_group Text NOT NULL); ";
            string inventory = "CREATE TABLE Inventory (id Integer PRIMARY KEY, store_id Integer NOT NULL, item_id Integer NOT NULL, floor_size Integer NOT NULL, on_floor_count Integer NOT NULL, in_back_count Integer NOT NULL);";
            string sales = "CREATE TABLE Sales ( id Integer PRIMARY KEY, item_id Integer NOT NULL, store_id Integer NOT NULL, sale_timestamp Text NOT NULL); ";
            string salvage = "CREATE TABLE Salvage ( id Integer PRIMARY KEY, item_id Integer NOT NULL, store_id Integer NOT NULL, salvage_timestamp Text NOT NULL); ";
            string delivery = "CREATE TABLE Delivery ( id Integer PRIMARY KEY NOT NULL, store_id Integer NOT NULL, delivery_timestamp Text NOT NULL); ";
            string delivered_item = "CREATE TABLE Delivered_Item ( id Integer PRIMARY KEY, delivery_id Integer NOT NULL, item_id Integer NOT NULL); ";

            IList<string> commands = new List<string> { store, item, inventory, sales, salvage, delivery, delivered_item };

            RunCommand(commands);
        }

        /// <summary>
        /// Drop the tables
        /// </summary>
        private void DropTables()
        {
            Console.WriteLine("dropping tables");

            string store = "DROP TABLE IF EXISTS Store";
            string item = "DROP TABLE IF EXISTS Item";
            string inventory = "DROP TABLE IF EXISTS Inventory";
            string sales = "DROP TABLE IF EXISTS Sales";
            string salvage = "DROP TABLE IF EXISTS Salvage";
            string delivery = "DROP TABLE IF EXISTS Delivery";
            string deliveredItem = "DROP TABLE IF EXISTS Delivered_Item";

            IList<string> commands = new List<string> { store, item, inventory, sales, salvage, delivery, deliveredItem };

            RunCommand(commands);
        }

        /// <summary>
        /// Insert data into the tables
        /// </summary>
        private void InsertData()
        {
            Console.WriteLine("inserting data");
            string store = "insert into Store values (1, 1)";

            List<string> commands = new List<string> { store };
            commands.AddRange(BuildItemInserts());
            commands.AddRange(BuildInventoryInserts());


            RunCommand(commands);
        }

        /// <summary>
        /// Insert data for testing into the tables
        /// </summary>
        private void InsertTestData()
        {
            //delivery 1
            string delivery1 = "insert into Delivery values (1, 1,'2019-09-15T15:53:00')";
            string delivered11 = "insert into Delivered_Item values (1, 1, 5)";
            string delivered12 = "insert into Delivered_Item values (2, 1, 34)";
            string delivered13 = "insert into Delivered_Item values (3, 1, 17)";            
            
            //delivery 2
            string delivery2 = "insert into Delivery values (2, 1,'2019-09-17T16:23:00')";
            string delivered21 = "insert into Delivered_Item values (4, 2, 23)";
            string delivered22 = "insert into Delivered_Item values (5, 2, 3)";
            string delivered23 = "insert into Delivered_Item values (6, 2, 48)";
            
            //sales
            string sales1 = "insert into Sales values (1, 34, 1, '2019-09-15T15:53:00')";
            string sales2 = "insert into Sales values (2, 2, 1, '2019-09-15T15:53:00')";
            string sales3 = "insert into Sales values (3, 49, 1, '2019-09-15T15:53:00')";

            string salvage = "insert into Salvage values (1, 35, 1, '2019-10-19T15:13:00')";

            List<string> commands = new List<string> { delivery1, delivered11, delivered12, delivered13,
                                                       delivery2, delivered21, delivered22, delivered23,
                                                       sales1, sales2, sales3,
                                                       salvage
            };
            RunCommand(commands);
        }



        // this is used mostly for debugging
        /// <summary>
        /// A basic health check for the database
        /// </summary>
        private void ReadData()
        {
            Console.WriteLine("reading data");

            var query = "select * from Store";

            RunQuery(query, ReadDataMapper);

        }

        /// <summary>
        /// A function to map the data from the store table
        /// </summary>
        /// <param name="sqLiteDataReader"></param>
        /// <returns></returns>
        private Boolean ReadDataMapper(SQLiteDataReader sqLiteDataReader)
        {
            int id = sqLiteDataReader.GetInt32(0);
            int number = sqLiteDataReader.GetInt32(1);
            Console.WriteLine(id + ", " + number);
            return true;
        }

        /// <summary>
        /// Insert the Items
        /// </summary>
        /// <returns>A list of item insert commands</returns>
        private IList<String> BuildItemInserts()
        {
            //id, name, fill group
            string chem1 =  "insert into Item values (1, 'bleach', 'CHEM'); ";
            string chem2 =  "insert into Item values (2, 'laundry detergent', 'CHEM'); ";
            string chem3 =  "insert into Item values (3, 'dish soap', 'CHEM'); ";
            string chem4 =  "insert into Item values (4, 'furniture polish', 'CHEM'); ";
            string chem5 =  "insert into Item values (5, 'air freshener', 'CHEM'); ";
            string chem6 =  "insert into Item values (6, 'raid', 'CHEM'); ";
            string chem7 =  "insert into Item values (7, 'tide', 'CHEM'); ";
            string chem8 =  "insert into Item values (8, 'gain', 'CHEM'); ";
            string chem9 =  "insert into Item values (9, 'downy', 'CHEM'); ";
            string chem10 = "insert into Item values (10, 'fabric softener', 'CHEM'); ";

            string paper11 = "insert into Item values (11, 'bounty 2 pack', 'PAPR'); ";
            string paper12 = "insert into Item values (12, 'bounty 6 pack', 'PAPR'); ";
            string paper13 = "insert into Item values (13, 'bounty 8 pack', 'PAPR'); ";
            string paper14 = "insert into Item values (14, 'charmin ultra soft', 'PAPR'); ";
            string paper15 = "insert into Item values (15, 'charmin ultra tough', 'PAPR'); ";
            string paper16 = "insert into Item values (16, 'paper bags', 'PAPR'); ";
            string paper17 = "insert into Item values (17, 'lunch bags', 'PAPR'); ";
            string paper18 = "insert into Item values (18, 'vivo 6 pack', 'PAPR'); ";
            string paper19 = "insert into Item values (19, 'paper towels', 'PAPR'); ";
            string paper20 = "insert into Item values (20, 'napkins', 'PAPR'); ";

            string baby21 = "insert into Item values (21, 'snug and dry diapers', 'BBO1'); ";
            string baby22 = "insert into Item values (22, 'diapers', 'BBO1'); ";
            string baby23 = "insert into Item values (23, 'ultra soft diapers', 'BBO1'); ";
            string baby24 = "insert into Item values (24, 'ultra tough diapers', 'BBO1'); ";
            string baby25 = "insert into Item values (25, 'bottles', 'BBO1'); ";
            string baby26 = "insert into Item values (26, 'pacifiers', 'BBO1'); ";
            string baby27 = "insert into Item values (27, 'crib', 'BBO1'); ";
            string baby28 = "insert into Item values (28, 'blanket', 'BBO1'); ";
            string baby29 = "insert into Item values (29, 'car seat', 'BBO1'); ";
            string baby30 = "insert into Item values (30, 'stroller', 'BBO1'); ";

            string home31 = "insert into Item values (31, 'blue plate', 'HOME'); ";
            string home32 = "insert into Item values (32, 'red plate', 'HOME'); ";
            string home33 = "insert into Item values (33, 'yellow plate', 'HOME'); ";
            string home34 = "insert into Item values (34, 'rug', 'HOME'); ";
            string home35 = "insert into Item values (35, 'plastic cup', 'HOME'); ";
            string home36 = "insert into Item values (36, 'microwave', 'HOME'); ";
            string home37 = "insert into Item values (37, 'blender', 'HOME'); ";
            string home38 = "insert into Item values (38, 'knife', 'HOME'); ";
            string home39 = "insert into Item values (39, 'fork', 'HOME'); ";
            string home40 = "insert into Item values (40, 'spoon', 'HOME'); ";

            string health41 = "insert into Item values (41, 'shampoo', 'HBO1'); ";
            string health42 = "insert into Item values (42, 'conditioner', 'HBO1'); ";
            string health43 = "insert into Item values (43, 'bath salts', 'HBO1'); ";
            string health44 = "insert into Item values (44, 'face wash', 'HBO1'); ";
            string health45 = "insert into Item values (45, 'toothbrush', 'HBO1'); ";
            string health46 = "insert into Item values (46, 'toothpaste', 'HBO1'); ";
            string health47 = "insert into Item values (47, 'deodorant', 'HBO1'); ";
            string health48 = "insert into Item values (48, 'sponge', 'HBO1'); ";
            string health49 = "insert into Item values (49, 'lipstick', 'HBO1'); ";
            string health50 = "insert into Item values (50, 'hairbrush', 'HBO1'); ";
            

            return new List<string> { chem1, chem2, chem3, chem4, chem5, chem6, chem7, chem8, chem9, chem10,
                                      paper11, paper12, paper13, paper14, paper15, paper16, paper17, paper18, paper19, paper20,
                                      baby21, baby22, baby23, baby24, baby25, baby26, baby27, baby28, baby29, baby30,
                                      home31, home32, home33, home34, home35, home36, home37, home38, home39, home40,
                                      health41, health42, health43, health44, health45, health46, health47, health48, health49, health50 };

        }

        /// <summary>
        /// build the inventory inserts
        /// </summary>
        /// <returns>a list of inventory insert commands</returns>
        private IList<String> BuildInventoryInserts()
        {
            //id, store id, item id, floor size, on floor count, in back count
            string item1  = "insert into Inventory values (1, 1, 1, 5, 3, 5); ";
            string item2  = "insert into Inventory values (2, 1, 2, 6, 4, 6); ";
            string item3  = "insert into Inventory values (3, 1, 3, 3, 2, 7); ";
            string item4  = "insert into Inventory values (4, 1, 4, 4, 3, 6); ";
            string item5  = "insert into Inventory values (5, 1, 5, 5, 4, 5); ";
            string item6  = "insert into Inventory values (6, 1, 6, 2, 2, 7); ";
            string item7  = "insert into Inventory values (7, 1, 7, 4, 2, 7); ";
            string item8  = "insert into Inventory values (8, 1, 8, 6, 2, 6); ";
            string item9  = "insert into Inventory values (9, 1, 9, 4, 3, 6); ";
            string item10 = "insert into Inventory values (10, 1, 10, 4, 2, 7); ";
                                         
            string item11 = "insert into Inventory values (11, 1, 11, 4, 2, 7); ";
            string item12 = "insert into Inventory values (12, 1, 12, 5, 2, 6); ";
            string item13 = "insert into Inventory values (13, 1, 13, 4, 4, 7); ";
            string item14 = "insert into Inventory values (14, 1, 14, 4, 2, 8); ";
            string item15 = "insert into Inventory values (15, 1, 15, 4, 3, 7); ";
            string item16 = "insert into Inventory values (16, 1, 16, 3, 2, 7); ";
            string item17 = "insert into Inventory values (17, 1, 17, 4, 3, 5); ";
            string item18 = "insert into Inventory values (18, 1, 18, 2, 2, 7); ";
            string item19 = "insert into Inventory values (19, 1, 19, 4, 2, 7); ";
            string item20 = "insert into Inventory values (20, 1, 20, 5, 4, 5); ";
                                         
            string item21 = "insert into Inventory values (21, 1, 21, 2, 1, 5); ";
            string item22 = "insert into Inventory values (22, 1, 22, 1, 1, 8); ";
            string item23 = "insert into Inventory values (23, 1, 23, 5, 4, 5); ";
            string item24 = "insert into Inventory values (24, 1, 24, 2, 2, 5); ";
            string item25 = "insert into Inventory values (25, 1, 25, 5, 4, 6); ";
            string item26 = "insert into Inventory values (26, 1, 26, 4, 4, 5); ";
            string item27 = "insert into Inventory values (27, 1, 27, 5, 4, 7); ";
            string item28 = "insert into Inventory values (28, 1, 28, 3, 3, 5); ";
            string item29 = "insert into Inventory values (29, 1, 29, 5, 4, 9); ";
            string item30 = "insert into Inventory values (30, 1, 30, 2, 2, 2); ";
                                         
            string item31 = "insert into Inventory values (31, 1, 31, 4, 2, 8); ";
            string item32 = "insert into Inventory values (32, 1, 32, 5, 3, 4); ";
            string item33 = "insert into Inventory values (33, 1, 33, 2, 2, 2); ";
            string item34 = "insert into Inventory values (34, 1, 34, 3, 3, 2); ";
            string item35 = "insert into Inventory values (35, 1, 35, 2, 2, 5); ";
            string item36 = "insert into Inventory values (36, 1, 36, 6, 2, 2); ";
            string item37 = "insert into Inventory values (37, 1, 37, 2, 1, 6); ";
            string item38 = "insert into Inventory values (38, 1, 38, 7, 5, 7); ";
            string item39 = "insert into Inventory values (39, 1, 39, 2, 2, 6); ";
            string item40 = "insert into Inventory values (40, 1, 40, 4, 2, 2); ";
                                         
            string item41 = "insert into Inventory values (41, 1, 41, 4, 2, 5); ";
            string item42 = "insert into Inventory values (42, 1, 42, 3, 3, 2); ";
            string item43 = "insert into Inventory values (43, 1, 43, 4, 3, 6); ";
            string item44 = "insert into Inventory values (44, 1, 44, 5, 4, 8); ";
            string item45 = "insert into Inventory values (45, 1, 45, 3, 2, 5); ";
            string item46 = "insert into Inventory values (46, 1, 46, 4, 3, 2); ";
            string item47 = "insert into Inventory values (47, 1, 47, 3, 3, 7); ";
            string item48 = "insert into Inventory values (48, 1, 48, 4, 2, 6); ";
            string item49 = "insert into Inventory values (49, 1, 49, 6, 6, 6); ";
            string item50 = "insert into Inventory values (50, 1, 50, 4, 2, 4); ";


            return new List<string> { item1, item2, item3, item4, item5, item6, item7, item8, item9, item10,
                                      item11, item12, item13, item14, item15, item16, item17, item18, item19, item20,
                                      item21, item22, item23, item24, item25, item26, item27, item28, item29, item30,
                                      item31, item32, item33, item34, item35, item36, item37, item38, item39, item40,
                                      item41, item42, item43, item44, item45, item46, item47, item48, item49, item50 };

        }
    }
}
