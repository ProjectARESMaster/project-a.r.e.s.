﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using ProjectARES.model;

namespace ProjectARES.dao
{
    public class InventoryDao : SQLiteDao
    {
        /// <summary>
        /// Get all the inventory for a store
        /// </summary>
        /// <param name="storeId">the store</param>
        /// <returns>List of inventory</returns>
        public IList<Inventory> GetInventoryByStore(int storeId)
        {
                //TODO - figure out substitution
            string query = " SELECT a.id,               " + //0
                           "        a.name,             " + //1
                           "        a.fill_group,       " + //2
                           "        b.id,               " + //3
                           "        b.store_id,         " + //4
                           "        b.floor_size,       " + //5
                           "        b.on_floor_count,   " + //6
                           "        b.in_back_count     " + //7
                           "   from Item a, Inventory b " +
                           "  where a.id = b.item_id    " +
                           "    and b.store_id =  " + storeId + ";";


            return RunQuery(query, GetInventoryMapper);

            
        }

        /// <summary>
        /// Get the inventory for a single item in a store
        /// </summary>
        /// <param name="itemId">the item</param>
        /// <param name="storeId">the store</param>
        /// <returns>The inventory</returns>
        public Inventory GetInventoryByItemId(int itemId, int storeId)
        {
            string query = " SELECT a.id,               " + //0
                           "        a.name,             " + //1
                           "        a.fill_group,       " + //2
                           "        b.id,               " + //3
                           "        b.store_id,         " + //4
                           "        b.floor_size,       " + //5
                           "        b.on_floor_count,   " + //6
                           "        b.in_back_count     " + //7
                           "   from Item a, Inventory b " +
                           "  where a.id = b.item_id    " +
                           "    and b.store_id =  " + storeId + 
                           "    and a.id =  " + itemId + ";";

            //ToDo: No element protection
            return RunQuery(query, GetInventoryMapper).First();
        }

        /// <summary>
        /// A function to map the query results to an Inventory
        /// </summary>
        /// <param name="dataReader">the data reader from the query</param>
        /// <returns>Inventory</returns>
        private Inventory GetInventoryMapper(SQLiteDataReader dataReader)
        {
            Item item = new Item(dataReader.GetInt32(0),
                                 dataReader.GetString(1),
                                 dataReader.GetString(2));

            return new Inventory(dataReader.GetInt32(3),
                                 dataReader.GetInt32(4),
                                 item,
                                 dataReader.GetInt32(5),
                                 dataReader.GetInt32(6),
                                 dataReader.GetInt32(7));
        }

        /// <summary>
        /// Update an inventory in the database
        /// </summary>
        /// <param name="inventory">The inventory to update</param>
        public void UpdateInventory(Inventory inventory)
        {
            UpdateInventory(new List<Inventory>{inventory});
        }

        /// <summary>
        /// Update inventories in the database
        /// </summary>
        /// <param name="inventoryList">The inventories to update</param>
        public void UpdateInventory(IList<Inventory> inventoryList)
        {
           var updates = new List<string>(inventoryList.Count);

            foreach (var inventory in inventoryList)
            {
                string query = " UPDATE Inventory " +
                               "    SET on_floor_count = " + inventory.OnFloorCount + ", " +
                               "        in_back_count = " + inventory.InBackCount + 
                               "  WHERE store_id = " + inventory.StoreId +
                               "    AND item_id = " + inventory.Item.Id + ";";

                updates.Add(query);
            }

            RunCommand(updates);
        }

        /// <summary>
        /// Delete an inventory from the database
        /// </summary>
        /// <param name="storeId">the store id</param>
        /// <param name="inventoryId">the inventory id</param>
        public void RemoveInventory(int storeId, int inventoryId)
        {
            string query = " delete from Inventory     " +
                           "       where store_id =    " + storeId +
                           "         and id =          " + inventoryId + ";";

            RunCommand(query);
        }

        /// <summary>
        /// Delete an inventory from the database
        /// </summary>
        /// <param name="inventory">the inventory to remove</param>
        public void RemoveInventory(Inventory inventory)
        {
            RemoveInventory(inventory.StoreId, inventory.Id);
        }

        /// <summary>
        /// insert new inventory into the database
        /// </summary>
        /// <param name="inventory">the inventor to insert</param>
        public void AddInventory(Inventory inventory)
        {
            RunInTransaction(inventory, InventoryInsert);
        }

        /// <summary>
        /// insert the item and the inventory
        /// </summary>
        /// <param name="inventory">the inventory to insert</param>
        /// <param name="connection">the db connection</param>
        private void InventoryInsert(Inventory inventory, SQLiteConnection connection)
        {
            InsertItem(inventory.Item, connection);

            //get the item id
            long itemId = connection.LastInsertRowId;

            InsertInventory(inventory, connection, itemId);
        }

        /// <summary>
        /// run the query to insert the inventory
        /// </summary>
        /// <param name="inventory">the inventory to insert</param>
        /// <param name="connection">the db connection</param>
        /// <param name="itemId">the id of the inventory's' item</param>
        private void InsertInventory(Inventory inventory, SQLiteConnection connection, long itemId)
        {
            string query = " insert into Inventory values ";

            string queryValues = query + buildInventoryInsert(inventory, itemId) + ";";

            RunCommand(queryValues, connection);
        }

        /// <summary>
        /// build the values clause for the inventory insert
        /// </summary>
        /// <param name="inventory"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        private string buildInventoryInsert(Inventory inventory, long itemId)
        {
            string insert = " ( null, {0}, {1}, {2}, {3}, {4} )";
            return string.Format(insert, inventory.StoreId, itemId, inventory.FloorSize, inventory.OnFloorCount,
                inventory.InBackCount);
        }
           
        /// <summary>
        /// run the query to insert an item
        /// </summary>
        /// <param name="item"></param>
        /// <param name="connection"></param>
        private void InsertItem(Item item, SQLiteConnection connection)
        {
            string query = "insert into Item  (name, fill_group) " +
                           " values ( \"{0}\", \"{1}\");";

            string queryValues = string.Format(query, item.Name, item.FillGroup);
            RunCommand(queryValues, connection);
        }
    }
}
