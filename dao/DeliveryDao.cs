﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using ProjectARES.model;

namespace ProjectARES.dao
{
    public class DeliveryDao : SQLiteDao
    {
        /// <summary>
        /// Get all the deliveries for a store
        /// </summary>
        /// <param name="storeId">the store</param>
        /// <returns>List of deliveries</returns>
        public IList<Delivery> GetDeliveriesByStore(int storeId)
        {
            string query = " select i.id,                   " + //0
                           "        i.name,                 " + //1
                           "        i.fill_group,           " + //2
                           "        di.id,                   " + //3
                           "        d.id,                   " + //4
                           "        d.store_id,             " + //5
                           "        d.delivery_timestamp   " + //6
                           "   from Item i, Delivered_Item di, Delivery d " +
                           "  where d.id = di.delivery_id   " +
                           "    and i.id = di.item_id   " +
                           "    and d.store_id = " + storeId + ";";

            var deliveries = RunQuery(query, GetDeliveriesByStoreMapper);

            //group delivered items into deliveries
            return deliveries.ToList()
                             .GroupBy(x => new Tuple<int, int, DateTime>(x.Item1, x.Item2, x.Item3), x => x.Item4)
                             .Select(group => new Delivery(group.Key.Item1, group.Key.Item2, group.Key.Item3, group.ToList()))
                             .ToList();

        }


        /// <summary>
        /// creates a delivered item tuple with the delivery info attached.
        /// </summary>
        /// <param name="dataReader">the data reader from the query</param>
        /// <returns></returns>
        private Tuple<int, int, DateTime, DeliveredItem> GetDeliveriesByStoreMapper(SQLiteDataReader dataReader)
        {
            Item item = new Item(dataReader.GetInt32(0),
                                 dataReader.GetString(1),
                                 dataReader.GetString(2));

            DeliveredItem deliveredItem = new DeliveredItem(dataReader.GetInt32(3),
                                                            item);
            
            //tuple with delivery id, store id, delivery time, delivered item
            return new Tuple<int, int, DateTime, DeliveredItem>(dataReader.GetInt32(4),
                                                                dataReader.GetInt32(5),
                                                                ToDateTime(dataReader.GetString(6)),
                                                                deliveredItem);
        }

        /// <summary>
        /// Insert a new delivery
        /// </summary>
        /// <param name="delivery">the delivery</param>
        public void AddDeliveries(Delivery delivery)
        {
            AddDeliveries(new List<Delivery>{delivery});
        }

        /// <summary>
        /// Insert new deliveries
        /// </summary>
        /// <param name="deliveries">the deliveries</param>
        public void AddDeliveries(IList<Delivery> deliveries)
        {
            foreach (var delivery in deliveries)
            {
                RunInTransaction(delivery, DeliveryInsert);
            }
        }

        /// <summary>
        /// run a delivery insert
        /// </summary>
        /// <param name="delivery"></param>
        /// <param name="connection"></param>
        private void DeliveryInsert(Delivery delivery, SQLiteConnection connection)
        {
            InsertDelivery(delivery, connection);

            //get the delivery id
            long deliveryId = connection.LastInsertRowId;

            InsertDeliveredItems(delivery, connection, deliveryId);
        }

        /// <summary>
        /// insert rows into the Delivered_Item table
        /// </summary>
        /// <param name="delivery">the delivery to insert</param>
        /// <param name="connection">a database connection</param>
        /// <param name="deliveryId">the id from the Delivery table</param>
        private void InsertDeliveredItems(Delivery delivery, SQLiteConnection connection, long deliveryId)
        {
            string query = " INSERT INTO Delivered_Item(delivery_id, item_id) " +
                           " VALUES ";

            var queryValues = string.Join(",", delivery.DeliveredItems.Select(x => BuildDeliveredItemInsert(x, deliveryId)).ToList());

            RunCommand(query + queryValues, connection);
        }

        /// <summary>
        /// build the values clause for a delivered item
        /// </summary>
        /// <param name="deliveredItem">the delivered item</param>
        /// <param name="deliveryId">the id from the Delivery table</param>
        /// <returns>the clause string</returns>
        private string BuildDeliveredItemInsert(DeliveredItem deliveredItem, long deliveryId)
        {
            return " ( " + deliveryId + ", " + deliveredItem.Item.Id + " ) ";
        }

        /// <summary>
        /// insert rows into the Delivery table
        /// </summary>
        /// <param name="delivery"></param>
        /// <param name="connection"></param>
        private void InsertDelivery(Delivery delivery, SQLiteConnection connection)
        {
            string query = " INSERT INTO Delivery(store_id, delivery_timestamp) " +
                           " VALUES ( " + delivery.StoreId + ", \"" + ToIsoDateString(DateTime.Now) + "\" )";

            RunCommand(query, connection);
        }

    }
}