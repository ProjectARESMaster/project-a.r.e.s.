﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace ProjectARES.dao
{
    public abstract class SQLiteDao
    {
        /// <summary>
        /// Create a database connection
        /// </summary>
        /// <returns>a connection</returns>
        protected SQLiteConnection CreateConnection()
        {
            // Create a new database connection:
            SQLiteConnection  sqLiteConnection = new SQLiteConnection("Data Source=ProjectARES.db; Version = 3; New = True; Compress = True; ");

            // Open the connection:
            try
            {
                sqLiteConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error getting database connection.");
                throw e;
            }
            return sqLiteConnection;
        }

        /// <summary>
        /// Runs a database query
        /// </summary>
        /// <typeparam name="R">The type of the result</typeparam>
        /// <param name="query">The query</param>
        /// <param name="resultMapper">A function that maps from SQLiteDataReader to a result of type R</param>
        /// <returns></returns>
        protected IList<R> RunQuery<R>(string query, Func<SQLiteDataReader, R> resultMapper)
        {
            using (SQLiteConnection connection = CreateConnection())
            {
                using (SQLiteCommand sqLiteCommand = new SQLiteCommand(query, connection))
                {
                    using (SQLiteDataReader dataReader = sqLiteCommand.ExecuteReader())
                    {
                        IList<R> results = new List<R>();

                        while (dataReader.Read())
                        {
                            var result = resultMapper(dataReader);

                            results.Add(result);
                        }

                        return results;
                    }
                }
            }
        }

        /// <summary>
        /// Run a list of commands
        /// <br/>
        /// Use this method for commands not in transactions.
        /// </summary>
        /// <param name="commands">The commands to run</param>
        protected void RunCommand(IList<string> commands)
        {
            using (SQLiteConnection connection = CreateConnection())
            {
                RunCommand(commands, connection);
            }
        }

        /// <summary>
        /// Run a single database command.
        /// <br/>
        /// Use this method for commands not in transactions.
        /// </summary>
        /// <param name="command">The command to run</param>
        protected void RunCommand(string command)
        {
            RunCommand(new List<string> { command });
        }

        /// <summary>
        /// Runs a database command.
        ///<br/>
        /// <b>Only</b> use this method if you are running commands in a <b>transaction</b>
        /// </summary>
        /// <param name="commands">The commands to run</param>
        /// <param name="connection">A database connection</param>
        protected void RunCommand(IList<string> commands, SQLiteConnection connection)
        {
            using (SQLiteCommand sqLiteCommand = connection.CreateCommand())
            {
                foreach (string command in commands)
                {
                    try
                    {
                        sqLiteCommand.CommandText = command;
                        sqLiteCommand.ExecuteNonQuery();
                    }
                    catch
                    {

                    }
                }
            }
        }

        /// <summary>
        /// Runs a database command.
        ///<br/>
        /// <b>Only</b> use this method if you are running a command in a <b>transaction</b>
        /// </summary>
        /// <param name="command">The command to run</param>
        /// <param name="connection">A database connection</param>
        protected void RunCommand(string command, SQLiteConnection connection)
        {
            RunCommand(new List<string>{command}, connection);
        }



        /// <summary>
        /// Run an action in a database transaction
        /// </summary>
        /// <typeparam name="T">The type of the input to the action command</typeparam>
        /// <param name="input">The input to the action command</param>
        /// <param name="action">the action to run in a transaction</param>
        public void RunInTransaction<T>(T input, Action<T, SQLiteConnection> action)
        {
            //this prevents the caller from having to manage the connection during a transaction
            using (SQLiteConnection connection = CreateConnection())
            {
                using (var cmd = new SQLiteCommand(connection))
                {
                    using (var transaction = connection.BeginTransaction())
                    {
                        action(input, connection);

                        transaction.Commit();
                    }
                }
            }
        }

        /// <summary>
        /// Convert the database date string to a DateTime
        /// </summary>
        /// <param name="dateTimeString">The string to convert</param>
        /// <returns></returns>
        protected DateTime ToDateTime(String dateTimeString)
        {
            return DateTime.Parse(dateTimeString, null, System.Globalization.DateTimeStyles.RoundtripKind);
        }

        /// <summary>
        /// Convert a DateTime to the database date string
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        protected string ToIsoDateString(DateTime date)
        {
            return date.ToString("s");
        }
    }
}
