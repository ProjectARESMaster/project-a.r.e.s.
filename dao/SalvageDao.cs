﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using ProjectARES.model;


 
namespace ProjectARES.dao
{
    /// <summary>
    /// this class is currently unused in the current version of the application
    /// </summary>
    public class SalvageDao : SQLiteDao
    {
        /// <summary>
        /// Get all the salvages for a store
        /// </summary>
        /// <param name="storeId">the store</param>
        /// <returns></returns>
        public IList<Salvage> GetSalvagesByStore(int storeId)
        {
            string query = " select i.id,               " + //0
                           "        i.name,             " + //1 
                           "        i.fill_group,       " + //2
                           "        s.id,               " + //3
                           "        s.store_id,         " + //4
                           "        s.salvage_timestamp " + //5
                           "   from Item i, Salvage s     " +
                           "  where i.id = s.item_id    " +
                           "    and s.store_id = " + storeId + ";";


            return RunQuery(query, GetSalvagesByStoreMapper);
        }

        /// <summary>
        /// A function to map the query results to a sale
        /// </summary>
        /// <param name="dataReader">the data reader from the query</param>
        /// <returns></returns>
        private Salvage GetSalvagesByStoreMapper(SQLiteDataReader dataReader)
        {
            Item item = new Item(dataReader.GetInt32(0),
                                 dataReader.GetString(1),
                                 dataReader.GetString(2));

            return new Salvage(dataReader.GetInt32(3),
                             item,
                             dataReader.GetInt32(4),
                             ToDateTime(dataReader.GetString(5)));
        }

        /// <summary>
        /// Insert new salvages into the database
        /// </summary>
        /// <param name="salvages">the sales to insert</param>
        public void InsertSalvages(IList<Salvage> salvages)
        {
            var query = " INSERT into Salvage(item_id, store_id, salvage_timestamp) " +
                        " VALUES ";

            var saleTime = DateTime.Now;

            var queryValues = string.Join(",", salvages.Select(x => BuildInsert(x, saleTime)).ToList());


            RunCommand(query + queryValues);
        }

        /// <summary>
        /// build the values clause for a Salvage
        /// </summary>
        /// <param name="salvage"></param>
        /// <param name="salvageTime"></param>
        /// <returns></returns>
        private string BuildInsert(Salvage salvage, DateTime salvageTime)
        {
            return " ( " + salvage.Item.Id + ", " + salvage.StoreId + ", \"" + ToIsoDateString(salvageTime) + "\" ) ";
        }
    }
}