using System;
using ProjectARES.dao;
using ProjectARES.controller;
using ProjectARES.utils;
using static ProjectARES.utils.MenuUtils;


namespace ProjectARES.view
{
    public class Menu
    {
        private const int storeId = 1;
        int userInput;

        /// <summary>
        /// This is to verify that input can be read
        /// </summary>
        /// <returns></returns>
        private static string tryReadLine()
        {
            int n = 0;
            string x = Console.ReadLine();
            var isValidNumber = Int32.TryParse(x,out n);
            if (!isValidNumber)
            {
                Console.WriteLine("Invalid input entered!");
                return "1000";
              
            }

            return x;


        }

        /// <summary>
        /// display the base menu
        /// </summary>
        public void DisplayMenu()
        {
            PrintMenuHeader();
            Console.WriteLine("1. Inventory Management");
            Console.WriteLine("2. Register System");
            Console.WriteLine("3. Pull List");
            Console.WriteLine("4. Truck Delivery");
            Console.WriteLine("5. Exit");
           
            userInput = Convert.ToInt32(tryReadLine());

            if (userInput == 1)
            {
                OptionOne();
            }
            else if (userInput == 2)
            {
                OptionTwo();
            }
            else if (userInput == 3)
            {
                OptionThree();
            }
            else if (userInput == 4)
            {
                OptionFour();
            }
            else if (userInput == 5)
            {
                Environment.Exit(0);
            }
            else if (userInput == 8)
            {
                new DatabaseBuilder().BuildDatabase();
                Console.Clear();
            }
            else
            {
                InvalidOption();
                PressAnyKey();
                DisplayMenu();
            }
        }

        /// <summary>
        /// display sub menu 1
        /// </summary>
        public void OptionOne()
        {
            PrintMenuHeader();
            Console.WriteLine("1. View Current Inventory");
            Console.WriteLine("2. Add Item to Inventory Database");
            Console.WriteLine("3. Remove Item from Inventory Database");
            Console.WriteLine("4. Go Back");
            Console.WriteLine("5. Exit Program");


            userInput = Convert.ToInt32(tryReadLine());



            if (userInput == 1)
            {
                Console.Clear();
                InventoryController inventoryController = new InventoryController();
                var results = inventoryController.Inventory(storeId);
                MenuUtils.DisplayInventoryVerbose(results);

                PressAnyKey();
            }
            if (userInput == 2)
            {
                Console.Clear();
                AddItemController addItemController = new AddItemController();
                var ItemName = GetItemDetailsItemName();
                var FillGroup = GetItemDetailsFillGroup();
                var SalesFloor = GetItemDetailsSalesFloor();
                var SalesFloorCount = GetItemDetailsSalesFloorCount();
                var InBackCount =  GetItemDetailsInBackCount();

                addItemController.InventoryDaoAdd(ItemName,FillGroup,SalesFloor,SalesFloorCount,InBackCount);

                PressAnyKey();
            }
            if (userInput == 3)
            {
                Console.Clear();
                SubtractItemController SubItemController = new SubtractItemController();
                var ItemID = GetItemDetailsItemID();
                SubItemController.InventoryDaoSub(ItemID);

                PressAnyKey();
            }
            else if (userInput == 4)
            {
                DisplayMenu();
            }
            else if (userInput == 5)
            {
                Environment.Exit(0);
            }
            else
            {
                InvalidOption();
                PressAnyKey();
                OptionOne();
            }
        }

        /// <summary>
        /// display sub menu 2
        /// </summary>
        public void OptionTwo()
        {
            PrintMenuHeader();
            Console.WriteLine("1. Buy Item");
            Console.WriteLine("2. Return Item");
            Console.WriteLine("3. Salvage Item");
            Console.WriteLine("4. Defect Item");
            Console.WriteLine("5. Go Back");
            Console.WriteLine("6. Exit Program");


            userInput = Convert.ToInt32(tryReadLine());


            if (userInput == 1)
            {
                RegisterController registerController = new RegisterController();
                var inventoryDao = new InventoryDao();
                Console.Clear();

                var itemId = GetItemToBuy(inventoryDao);
                var itemQuantity = GetItemQuantity();

                string message = registerController.BuyItem(itemId, itemQuantity, 1);
                Console.WriteLine(message);

                PressAnyKey();
                OptionTwo();

            }
            else if (userInput == 2)
            {
                ReturnController ReturnController = new ReturnController();
                var inventoryDao = new InventoryDao();
                Console.Clear();

                var itemId = GetItemToReturn(inventoryDao);
                var itemQuantity = GetItemQuantityReturn();

                string message = ReturnController.ReturnItem(itemId, itemQuantity, 1);
                Console.WriteLine(message);

                PressAnyKey();
                OptionTwo();
            }
            else if (userInput == 3)
            {
                SalvageController SalvageController = new SalvageController();
                var inventoryDao = new InventoryDao();
                Console.Clear();

                var itemId = GetItemToSalvage(inventoryDao);
                var itemQuantity = GetItemQuantitySalvage();

                string message = SalvageController.SalvageItem(itemId, itemQuantity, 1);
                Console.WriteLine(message);

                PressAnyKey();
                OptionTwo();
            }
            else if (userInput == 4)
            {
                DefectController DefectController = new DefectController();
                var inventoryDao = new InventoryDao();
                Console.Clear();

                var itemId = GetItemToDefect(inventoryDao);
                var itemQuantity = GetItemQuantityDefect();

                string message = DefectController.DefectItem(itemId, itemQuantity, 1);
                Console.WriteLine(message);

                PressAnyKey();
                OptionTwo();
            }
            else if (userInput == 5)
            {
                Console.Clear();
                DisplayMenu();
            }
            else if (userInput == 6)
            {
                Environment.Exit(0);
            }
            else
            {
                InvalidOption();
                PressAnyKey();
                OptionTwo();
            }

        }

        /// <summary>
        /// display sub menu 3
        /// </summary>
        public void OptionThree()
        {
            PrintMenuHeader();
            Console.WriteLine("1. Generate Pull List");
            Console.WriteLine("2. Organized Pull List");
            Console.WriteLine("3. Go Back");
            Console.WriteLine("4. Exit Program");

            userInput = Convert.ToInt32(tryReadLine());


            if (userInput == 1)
            {
                Console.Clear();
                PullController pullController = new PullController();
                var results = pullController.RunPulls(storeId);
                MenuUtils.DisplayInventoryVerbose(results);

                PressAnyKey();
            }
            else if (userInput == 2)
            {
                Console.Clear();
                PullControllerFillGroup pullController = new PullControllerFillGroup();
                InventoryUtils Checks = new InventoryUtils();

                var itemfillgroup = GetItemDetailsFillGroup();
                var check = InventoryUtils.isValidFillGroup(itemfillgroup);
                Console.WriteLine(check);
                if(check == "Error: Item Fillgroup does not exist!")
                {
                    GetItemDetailsFillGroup();
                }
                var results = pullController.RunSortPulls(storeId, itemfillgroup);
                MenuUtils.DisplayInventoryVerbose(results);



                PressAnyKey();
            }
            else if (userInput == 3)
            {
                Console.Clear();
                DisplayMenu();
            }
            else if (userInput == 4)
            {
                Environment.Exit(0);
            }
            else
            {
                InvalidOption();
                PressAnyKey();
                OptionThree();
            }
        }

        /// <summary>
        /// display sub menu 4
        /// </summary>
        public void OptionFour()
        {
            PrintMenuHeader();
            Console.WriteLine("1. Generate Truck");
            Console.WriteLine("2. Go Back");
            Console.WriteLine("3. Exit Program");

            userInput = Convert.ToInt32(tryReadLine());


            if (userInput == 1)
            {
                Console.Clear();
                DeliveryController deliveryController = new DeliveryController();
                var results = deliveryController.DeliveryTruck(storeId);

                MenuUtils.DisplayDelivery(results);

                PressAnyKey();
            }
            else if (userInput == 2)
            {
                Console.Clear();
                DisplayMenu();
            }
            else if (userInput == 3)
            {
                Console.Clear();
                Environment.Exit(0);
            }
            else
            {
                Environment.Exit(0);
            }
        }


        /// <summary>
        /// display that the option was invalid
        /// </summary>
        private void InvalidOption()
        {
            Console.WriteLine("Error: Please choose a valid option");
        }
        /// <summary>
        /// Prompt user for the Item Details
        /// </summary>
        /// <delete></deleteitem>
        private static int GetItemDetailsItemID()
        {
            Console.Write("\n\n");
            Console.WriteLine("What is the Item ID of the Item you would like to delete?");
            var ItemID = Convert.ToInt32(tryReadLine());
            Console.Clear();
            return ItemID;
        }
        private static int GetItemDetailsItemFillGroup()
        {
            Console.Write("\n\n");
            Console.WriteLine("What is the Item Fillgroup that you wish to pull for?");
            var ItemFillGroup = Convert.ToInt32(tryReadLine());
            Console.Clear();
            return ItemFillGroup;
        }
        /// <summary>
        /// Prompt user for the Item Details
        /// </summary>
        /// <returns></returns>
        private static string GetItemDetailsItemName()
        {
            Console.Write("\n\n");
            Console.WriteLine("What is the Item Name of the Item you would like to create?");
            var ItemName = Console.ReadLine();
            Console.Clear();
            return ItemName;
        }
        private static string GetItemDetailsFillGroup()
        {
            Console.Write("\n\n");
            Console.WriteLine("What is the Fill-Group of the Item you would like to create?");
            string FillGroup = Console.ReadLine();
            Console.Clear();
            return FillGroup;
        }
        private static int GetItemDetailsSalesFloor()
        {
            Console.WriteLine("How many fit on the Sales Floor?");
            var SalesFloor = Convert.ToInt32(tryReadLine());
            Console.Clear();
            return SalesFloor;
        }
        private static int GetItemDetailsSalesFloorCount()
        {
            Console.WriteLine("How many are on the Sales Floor?");
            var SalesFloorCount = Convert.ToInt32(tryReadLine());
            Console.Clear();
            return SalesFloorCount;
        }
        private static int GetItemDetailsInBackCount()
        {
            Console.WriteLine("How many are in the back?");
            var BackCount = Convert.ToInt32(tryReadLine());
            Console.Clear();
            return BackCount;
        }
        /// <summary>
        /// prompt the user for an item quantity 
        /// </summary>
        /// <returns></returns>
        private static int GetItemQuantity()
        {
            Console.Write("\n\n");
            Console.WriteLine("How many would you like to buy?");
            int itemQuantity = Convert.ToInt32(tryReadLine());
            return itemQuantity;
        }
        /// </summary>
        /// <returns></returns>
        private static int GetItemQuantityReturn()
        {
            Console.Write("\n\n");
            Console.WriteLine("How many would you like to Return?");
            int itemQuantity = Convert.ToInt32(tryReadLine());
            return itemQuantity;
        }
        /// </summary>
        /// <salvage/salvage>
        private static int GetItemQuantitySalvage()
        {
            Console.Write("\n\n");
            Console.WriteLine("How many would you like to Salvage?");
            int itemQuantity = Convert.ToInt32(tryReadLine());
            return itemQuantity;
        }
        /// </summary>
        /// <defect/defect>
        private static int GetItemQuantityDefect()
        {
            Console.Write("\n\n");
            Console.WriteLine("How many would you like to Defect?");
            int itemQuantity = Convert.ToInt32(tryReadLine());
            return itemQuantity;
        }

        /// <summary>
        /// prompt the user for an item to buy and display its inventory
        /// </summary>
        /// <param name="inventoryDao">the inventory dao</param>
        /// <returns>the id of the item</returns>
        private static int GetItemToBuy(InventoryDao inventoryDao)
        {
            Console.Write("\n\n");
            Console.WriteLine("What is the item id you would like to buy?");
            int itemId = Convert.ToInt32(tryReadLine());
            var result = inventoryDao.GetInventoryByItemId(itemId, 1);
            MenuUtils.DisplayInventory(result);
            return itemId;
        }
        /// <summary>
        /// prompt the user for an item to return and display its inventory
        /// </summary>
        /// <param name="inventoryDao">the inventory dao</param>
        /// <returns>the id of the item</returns>
        private static int GetItemToReturn(InventoryDao inventoryDao)
        {
            Console.Write("\n\n");
            Console.WriteLine("What is the item id you would like to return?");
            int itemId = Convert.ToInt32(tryReadLine());
            var result = inventoryDao.GetInventoryByItemId(itemId, 1);
            MenuUtils.DisplayInventory(result);
            return itemId;
        }
        /// <summary>
        /// prompt the user for an item to salvage and display its inventory
        /// </summary>
        /// <param name="inventoryDao">the inventory dao</param>
        /// <returns>the id of the item</returns>
        private static int GetItemToSalvage(InventoryDao inventoryDao)
        {
            Console.Write("\n\n");
            Console.WriteLine("What is the item id you would like to salvage?");
            int itemId = Convert.ToInt32(tryReadLine());
            var result = inventoryDao.GetInventoryByItemId(itemId, 1);
            MenuUtils.DisplayInventory(result);
            return itemId;
        }
        /// <summary>
        /// prompt the user for an item to salvage and display its inventory
        /// </summary>
        /// <param name="inventoryDao">the inventory dao</param>
        /// <returns>the id of the item</returns>
        private static int GetItemToDefect(InventoryDao inventoryDao)
        {
            Console.Write("\n\n");
            Console.WriteLine("What is the item id you would like to Defect?");
            int itemId = Convert.ToInt32(tryReadLine());
            var result = inventoryDao.GetInventoryByItemId(itemId, 1);
            MenuUtils.DisplayInventory(result);
            return itemId;
        }
        /// <summary>
        /// print the menu header
        /// </summary>
        private void PrintMenuHeader()
        {
            Console.Clear();
            Console.Write("\n\n");
            Console.Write("Project A.R.E.S.\n");
            Console.Write("------------------------------------------------");
            Console.Write("\n\n");
            Console.Write("Menu Options");
            Console.Write("\n\n");
        }



    }
}