﻿using System;
using System.Collections.Generic;
using ProjectARES.model;

namespace ProjectARES.utils
{
    public class MenuUtils
    {

        /// <summary>
        /// prompt for press any key to continue
        /// </summary>
        public static void PressAnyKey()
        {
            Console.WriteLine("\n\n");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }


        /// <summary>
        /// print a user formatted inventory to the console
        /// <br/>
        /// displays Item Name, Item Id, Quantity
        /// </summary>
        /// <param name="inventory">the inventory to print</param>
        public static void DisplayInventory(Inventory inventory)
        {
            DisplayInventory(new List<Inventory>{inventory});
        }

        /// <summary>
        /// print a user formatted inventory to the console
        /// </summary>
        /// <param name="inventories">the inventory to print</param>
        public static void DisplayInventory(IList<Inventory> inventories)
        {

            Console.WriteLine("");
            Console.WriteLine("Item Name".PadRight(25) + " " + "Item Id".PadRight(7) + " " + "Qty".PadRight(4));
            Console.WriteLine("".PadRight(25, '-') + " " + "".PadRight(7, '-') + " " + "".PadRight(4, '-'));

            foreach (var inventory in inventories)
            {
                PrintInventory(inventory);
            }
        }

        /// <summary>
        /// print a user formatted item to the console
        /// </summary>
        /// <param name="inventory"></param>
        private static void PrintInventory(Inventory inventory)
        {
            Console.WriteLine(inventory.Item.Name.PadRight(25) + " " + inventory.Item.Id.ToString().PadLeft(7) + " " + inventory.getOnHand().ToString().PadLeft(4));
        }

        /// <summary>
        /// print a verbose user formatted inventory to the console
        /// <br/>
        /// displays Item Name, Item Id, Floor Size, On Floor Count, In Back Count
        /// </summary>
        /// <param name="inventory">the inventory to print</param>
        public static void DisplayInventoryVerbose(Inventory inventory)
        {
            DisplayInventoryVerbose(new List<Inventory> { inventory });
        }

        /// <summary>
        /// print a verbose user formatted inventory to the console
        /// <br/>
        /// displays Item Name, Item Id, Floor Size, On Floor Count, In Back Count
        /// </summary>
        /// <param name="inventories">the inventory to print</param>
        public static void DisplayInventoryVerbose(IList<Inventory> inventories)
        {

            Console.WriteLine("");
            Console.WriteLine("Item Name".PadRight(25) + " " + "Item Id".PadRight(7) + " " + "Floor Size".PadRight(10) + " " + "Floor Cnt".PadRight(9) + " " + "Back Cnt".PadRight(8));
            Console.WriteLine("".PadRight(25, '-') + " " + "".PadRight(7, '-') + " " + "".PadRight(10, '-') + " " + "".PadRight(9, '-') + " " + "".PadRight(8, '-'));

            foreach (var inventory in inventories)
            {
                PrintInventoryVerbose(inventory);
            }
        }

        /// <summary>
        /// print the verbose inventory to the console
        /// </summary>
        /// <param name="inventory"></param>
        private static void PrintInventoryVerbose(Inventory inventory)
        {
            Console.WriteLine(inventory.Item.Name.PadRight(25) + " " + inventory.Item.Id.ToString().PadLeft(7) + " " + inventory.FloorSize.ToString().PadLeft(10) + " " +
                              inventory.OnFloorCount.ToString().PadLeft(9) + " " + inventory.InBackCount.ToString().PadLeft(8));
        }

        /// <summary>
        /// print a user formatted delivery to the console
        /// </summary>
        /// <param name="delivery">the delivery to print</param>
        public static void DisplayDelivery(Delivery delivery)
        {
            Console.WriteLine("\nDelivery");
            Console.WriteLine("---------------------------------------------------------------------------");
            Console.WriteLine("Store: {0} \nDelivery Time: {1}", delivery.StoreId, delivery.DeliveryTime);
            Console.WriteLine("\nDelivered Items:");

            foreach (var deliveredItem in delivery.DeliveredItems)
            {
                PrintDeliveredItem(deliveredItem);
            }

        }

        /// <summary>
        /// print a user formatted delivered item to the console
        /// </summary>
        /// <param name="deliveredItem">the delivered item to print</param>
        private static void PrintDeliveredItem(DeliveredItem deliveredItem)
        {
            Console.WriteLine("\t{0}", deliveredItem.Item.Name);
        }
    }
}