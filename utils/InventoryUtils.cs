﻿using System.Collections.Generic;
using ProjectARES.model;
using ProjectARES.dao;
using ProjectARES.controller;
using ProjectARES.utils;
using System.Linq;

namespace ProjectARES.utils
{
    public class InventoryUtils
    {
        /// <summary>
        /// check to see if fillgroup is valid
        /// </summary>
        /// <param name="inventory">the inventory to check</param>
        /// <returns>the availability status</returns>
        public static string isValidFillGroup(string userinput)
        {
            var inventoryDao = new InventoryDao();
            IList<Inventory> inventoryList = inventoryDao.GetInventoryByStore(1);


            bool contains = inventoryList.Any(s => s.Item.FillGroup.Contains(userinput));
            if (inventoryList.Any(str => str.Item.FillGroup.Contains(userinput)))
            {
                return ("Item Fillgroup exists!");
            }
            else
                return ("Error: Item Fillgroup does not exist!");
        }


        /// <summary>
        /// check to see if an item is available
        /// </summary>
        /// <param name="inventory">the inventory to check</param>
        /// <returns>the availability status</returns>
        public static bool IsItemAvailable(Inventory inventory)
        {
            return inventory.getOnHand() > 0;
        }

        /// <summary>
        /// check to see if an item is available in the specified quantity 
        /// </summary>
        /// <param name="inventory">the inventory to check</param>
        /// <param name="quantity">the quantity desired</param>
        /// <returns>the availability status</returns>
        public static bool IsItemAvailable(Inventory inventory, int quantity)
        {
            return inventory.getOnHand() > quantity;
        }

        /// <summary>
        /// Removes the item form inventory until the desired quantity is reached or it is out of stock
        /// </summary>
        /// <param name="inventory">the inventory to remove</param>
        /// <param name="quantity">the quantity to remove</param>
        /// <returns>The number of items removed</returns>
        public static int RemoveItemFromInventory(Inventory inventory, int quantity)
        {
            int itemsRemovedCount = 0;

            //remove on floor items
            while (inventory.OnFloorCount > 0 && itemsRemovedCount < quantity)
            {
                itemsRemovedCount++;
                inventory.OnFloorCount--;
            }

            //remove in back items
            while (inventory.InBackCount > 0 && itemsRemovedCount < quantity)
            {
                itemsRemovedCount++;
                inventory.InBackCount--;
            }

            return itemsRemovedCount;
        }

        /// <summary>
        /// restock items from the back to the front
        /// </summary>
        /// <param name="inventory">The inventory to pull</param>
        /// <returns>was the item pulled</returns>
        public static bool PullInventory(Inventory inventory)
        {
            bool wasPulled = false;

            while (inventory.OnFloorCount < inventory.FloorSize && inventory.InBackCount > 0)
            {
                inventory.OnFloorCount++;
                inventory.InBackCount--;
                wasPulled = true;
            }

            return wasPulled;
        }

        /// <summary>
        /// restock items from the back to the front
        /// </summary>
        /// <param name="inventoryList">the items to pull</param>
        /// <returns>a list of items with pulled inventory</returns>
        public static IList<Inventory> PullInventory(IList<Inventory> inventoryList)
        {
            IList<Inventory> pulledInventories = new List<Inventory>();

            foreach (var inventory in inventoryList)
            {
                if (PullInventory(inventory))
                {
                    pulledInventories.Add(inventory);
                }
            }

            return pulledInventories;
        }
      

    }
}