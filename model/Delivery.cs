﻿using System;
using System.Collections.Generic;

namespace ProjectARES.model
{
    /// <summary>
    /// holds a delivery
    /// </summary>
    public class Delivery
    {
        public int Id { get; }
        public int StoreId { get; }
        public DateTime DeliveryTime { get; }

        public IList<DeliveredItem> DeliveredItems { get; }

        public Delivery(int id, int storeId, DateTime deliveryTime, IList<DeliveredItem> deliveredItems)
        {
            this.Id = id;
            this.StoreId = storeId;
            this.DeliveryTime = deliveryTime;
            this.DeliveredItems = deliveredItems;
        }

        public Delivery(int storeId, DateTime deliveryTime, IList<DeliveredItem> deliveredItems)
        {
            StoreId = storeId;
            DeliveryTime = deliveryTime;
            DeliveredItems = deliveredItems;
        }

        public override string ToString()
        {
            return $"Delivery= id: {Id}, storeId: {StoreId}, deliveryTime: {DeliveryTime}, deliveredItems:{{ {string.Join("}, {", DeliveredItems)} }}";
        }
    }
}