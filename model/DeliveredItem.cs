﻿namespace ProjectARES.model
{
    /// <summary>
    /// holds a delivered item
    /// </summary>
    public class DeliveredItem
    {
        public int Id { get; }
        public Item Item { get; }

        public DeliveredItem(int id, Item item)
        {
            Id = id;
            Item = item;
        }

        public DeliveredItem(Item item)
        {
            Item = item;
        }

        public override string ToString()
        {
            return $"Id: {Id}, Item: {Item}";
        }
    }
}