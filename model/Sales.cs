﻿using System;

namespace ProjectARES.model
{
    /// <summary>
    /// holds sales
    /// </summary>
    public class Sales
    {
        public int Id { get; }
        public Item Item { get; }
        public int StoreId { get; }
        public DateTime SaleTime { get; }


        public Sales(int id, Item item, int storeId, DateTime saleTime)
        {
            Id = id;
            Item = item;
            StoreId = storeId;
            SaleTime = saleTime;
        }

        public Sales(Item item, int storeId, DateTime saleTime)
        {
            Item = item;
            StoreId = storeId;
            SaleTime = saleTime;
        }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(Item)}: {Item}, {nameof(StoreId)}: {StoreId}, {nameof(SaleTime)}: {SaleTime}";
        }
    }
}