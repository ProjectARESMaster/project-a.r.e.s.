﻿
namespace ProjectARES.model
{
    /// <summary>
    /// holds an Inventory
    /// </summary>
    public class Inventory
    {
        public int Id { get; }
        public int StoreId {get; }
        public Item Item {get; }
        public int FloorSize { get; set; }
        public int OnFloorCount { get; set; }
        public int InBackCount {  get; set; }

        public Inventory(int id, int storeId, Item item, int floorSize, int onFloorCount, int inBackCount)
        {
            Id = id;
            StoreId = storeId;
            Item = item;
            FloorSize = floorSize;
            OnFloorCount = onFloorCount;
            InBackCount = inBackCount;
        }

        public Inventory(int storeId, Item item, int floorSize, int onFloorCount, int inBackCount)
        {
            StoreId = storeId;
            Item = item;
            FloorSize = floorSize;
            OnFloorCount = onFloorCount;
            InBackCount = inBackCount;
        }

        public int getOnHand()
        {
            return OnFloorCount + InBackCount;
        }

        public override string ToString()
        {
            return $"Id: {Id}, StoreId: {StoreId}, Item: {Item}, FloorSize: {FloorSize}, OnFloorCount: {OnFloorCount}, InBackCount: {InBackCount}";
        }
    }
}
