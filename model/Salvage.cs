﻿using System;

namespace ProjectARES.model
{
    /// <summary>
    /// holds salvages
    /// </summary>
    ///
    /// this class is currently unused in the current version of the application

    public class Salvage
    {
        public int Id { get; }
        public Item Item { get; }
        public int StoreId { get; }
        public DateTime SalvageTime { get; }


        public Salvage(int id, Item item, int storeId, DateTime salvageTime)
        {
            Id = id;
            Item = item;
            StoreId = storeId;
            SalvageTime = salvageTime;
        }

        public Salvage(Item item, int storeId, DateTime salvageTime)
        {
            Item = item;
            StoreId = storeId;
            SalvageTime = salvageTime;
        }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(Item)}: {Item}, {nameof(StoreId)}: {StoreId}, {nameof(SalvageTime)}: {SalvageTime}";
        }
    }

}