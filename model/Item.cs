﻿

namespace ProjectARES.model
{
    /// <summary>
    /// holds an item
    /// </summary>
    public class Item
    {
        public int Id { get; }
        public string Name { get; }
        public string FillGroup { get; }

        public Item(int id, string name, string fillGroup)
        {
            Id = id;
            Name = name;
            FillGroup = fillGroup;
        }

        /// <summary>
        /// Us this one for new items
        /// </summary>
        /// <param name="name">the name</param>
        /// <param name="fillGroup">the fill group</param>
        public Item(string name, string fillGroup)
        {
            Name = name;
            FillGroup = fillGroup;
        }

        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name}, FillGroup: {FillGroup}";
        }
    }
}
